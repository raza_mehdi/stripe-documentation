<?php

namespace App\Http\Controllers;
use App\Subscription;
use App\SubscriptionHistory;
use Illuminate\Support\Facades\Log;
use Config;
use App\User;

use Illuminate\Http\Request;

class WebHookActivityController extends Controller
{
    public function webHookActivity(Request $request)
    {
        //signing secret
        $endpoint_secret =  Config::get('constants.stripe.signInSecret');

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        }

        $obj = $event->data->object; // contains a \Stripe\PaymentIntent

        switch ($event->type) {
            case 'charge.succeeded':
                Log::channel('stripe')->info(['payment succeed response' => $obj]);
                $user = User::whereStripeId($obj['customer'])->first();
                if($user)
                {
                    $subscription = Subscription::where(['user_id' => $user->id, 'stripe_status' => 'active'])->whereNull('ends_at')->first();
                    // deactivate previous subscription to maintain subscription history
                    SubscriptionHistory::where('user_id', $user->id)->update(['active' => 'NO']);
                    // Log::channel('stripe')->info(['plan name' => $subscription->name]);
                    // create new subscription entry
                    SubscriptionHistory::create([
                        'user_id'       => $user->id,
                        'name'          => $subscription->name,
                        'stripe_id'     => $subscription->stripe_id,
                        'payment_id'    => $obj['id'],
                        'subtotal'      => $obj['amount'],
                        'tax'           => 0,
                        'total'         => $obj['amount'],
                        'receipt_url'   => $obj['receipt_url'],
                        'company_id'    => $user->company_id,
                    ]);
                    Log::channel('stripe')->info(['stripe payment has been successfully added against user_id' => $user->id]);
                }
                break;
            case 'charge.failed':
                $user = User::whereStripeId($obj['customer'])->first();
                Log::channel('stripe')->info(['charge failed against user' => $obj['customer']]);
                if($user)
                {
                    $subscription = Subscription::where(['user_id' => $user->id, 'stripe_status' => 'active'])->whereNull('ends_at')->first();
                    if($subscription)
                    {
                        $user->subscription($subscription->name)->cancelNow();
                        Log::channel('stripe')->info(['stripe payment failed against user_id' => $user->id]);
                    }
                } else {
                    Log::channel('stripe')->info(['stripe payment failed due to invalid card details']);
                }
                break;
            case 'customer.subscription.deleted':
                Subscription::where('stripe_id', $obj['id'])->update(['stripe_status' => 'canceled', 'ends_at' => now()]);
                Log::channel('stripe')->info(['subscription cancelled against subscription_id' => $obj['id']]);
                break;
            default:
                Log::channel('stripe')->info(['Received unknown event type ' . $event->type]);
        }
        http_response_code(200);
    }
}
