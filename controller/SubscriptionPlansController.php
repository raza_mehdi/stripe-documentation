<?php

namespace App\Http\Controllers;

use App\SubscriptionPlan;
use App\Currency;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Config;
use Stripe;
use App\User;
use Illuminate\Support\Facades\DB;

class SubscriptionPlansController extends Controller
{
    public function __construct()
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PlanRequest $request)
    {
        $nameData['billing_type'] = $request->billing_type['name'];
        $currency_code = Currency::findOrFail($request->currency['id']);

        $stripe = new \Stripe\StripeClient(
            Config::get('constants.stripe.secret')
          );

        $product = $stripe->products->create([
        'name' => $request->name,
        'description' => $request->description,
        ]);

        if($nameData['billing_type'] == 'One Time'){

            $duration= [
                'unit_amount' => $request->price*100,
                'currency' => strtolower($currency_code['code']),
                'product' => $product->id
            ];
        }else{
            // $duration =  "['recurring' => ['interval' => 'month']]";
            $duration=  [
                'unit_amount' => $request->price*100,
                'currency' => strtolower($currency_code['code']),
                'recurring' => ['interval' => 'month'],
                'product' => $product->id
            ];
        }

        if(isset($product->id)){
          $price =   $stripe->prices->create([$duration]);
        }

        if(isset($price->id)){
            $subscription = new SubscriptionPlan();

            $subscription->name = $request->name;
            $subscription->price = $request->price*100;
            $subscription->description = $request->description;
            $subscription->billing_type = $request->billing_type['name'];
            // $subscription->pricing_modal = $request->pricing_modal['name'];
            // if(isset($request->billing_period['name'])){
            //     $subscription->billing_period = $request->billing_period['name'];
            // }else{
            //     $subscription->billing_period = NULL;
            // }

            if(isset($price->recurring)){
                $subscription->billing_period = $price->recurring->interval;
            }

            $subscription->currency_id = $request->currency['id'];
            // $subscription->company_id = $request->header('company');
            $subscription->stripe_price_id = $price->id;
            $subscription->stripe_product_id = $product->id;

            $subscription->save();
        }
        $subscription = SubscriptionPlan::find($subscription->id);

        return response()->json([
            'status' => 200,
            'success' => true,
            'plans' => $subscription
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubscriptionPlan  $subscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function show(SubscriptionPlan $subscriptionPlan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubscriptionPlan  $subscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $subscription = SubscriptionPlan::findorFail($id);
        return response()->json([
            'status' => 200,
            'success' => true,
            'plans' => $subscription
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubscriptionPlan  $subscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PlanRequest $request, $id)
    {
        $nameData['billing_type'] = $request->billing_type['name'];
        // if ($request->billing_type['name'] != NULL) {
        //     $validator = Validator::make($nameData, [
        //         'billing_period' => 'required',
        //     ]);
        // }

        $subscription = SubscriptionPlan::findorFail($id);

        $stripe = new \Stripe\StripeClient(
            Config::get('constants.stripe.secret')
          );

        $product = $stripe->products->update(
            $subscription->stripe_product_id,
            [
                'name' => $request->name,
                'description' => $request->description,
            ]
        );

        if(isset($product->id)){

            $subscription->name = $request->name;
            $subscription->price = $request->price*100;
            $subscription->description = $request->description;
            // $subscription->billing_type = $request->billing_type['name'];
            // $subscription->pricing_modal = $request->pricing_modal['name'];
            // if(isset($request->billing_period['name'])){
            //     $subscription->billing_period = $request->billing_period['name'];
            // }else{
            //     $subscription->billing_period = NULL;
            // }

            $subscription->currency_id = $request->currency['id'];
            // $subscription->company_id = $request->header('company');
            $subscription->stripe_price_id = $subscription->stripe_price_id;
            $subscription->stripe_product_id = $product->id;

            $subscription->save();
        }

        return response()->json([
            'status' => 200,
            'success' => true,
            'plans' => $subscription
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubscriptionPlan  $subscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $stripe = new \Stripe\StripeClient(
            Config::get('constants.stripe.secret')
          );

          
        $stripe->products->delete(
            $id,
            []
          );
    }
}
