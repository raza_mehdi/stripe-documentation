<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanySetting;
use App\Helpers\Email;
use App\Mail\subscriptionFailed;
use App\SubscriptionPlan;
use Illuminate\Http\Request;
Use App\User;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
Use App\Subscription;
use Session;
use Exception;
use App\Setting;
use App\SubscriptionHistory;

class SubscriptionController extends Controller
{
    /**
     * Display a payment page.
     *
     * @return \Illuminate\Http\JsonResponse 
     */
    public function index(Request $request)
    {
       
        $user = User::Find($request->id);

        /*redirect for unauthenticated users*/
        if(!$user)
        {
            return redirect()->to(url('/system/dashboard'));
        }

        $plans = SubscriptionPlan::with('currency')->whereNotIn('name', ['Free'])->get();
        $intent = $user->createSetupIntent();
        return view('subscription.create', compact('plans', 'intent'));
    }

    /**
     * Subscribe for stripe subscription after successfull payment
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkout (Request $request)
    {
        $user = User::whereCompanyId($request->company_id)->whereRole('subadmin')->first();
        $input = $request->all();
        $paymentPayload = $request->input('payment-method');

        $plan = SubscriptionPlan::whereStripePriceId($input['plan'])->first();

        try {
            /*creating user on stripe*/
            if (is_null($user->stripe_id)) {
                $user->createAsStripeCustomer();
            }

            /*creating subscription for user*/
            $user->newSubscription($plan->name, $input['plan'])
                ->create($paymentPayload, [
                    'email' => $user->email,
                ]);

            /*update card expiry*/
            $paymentMethod = $user->defaultPaymentMethod();

//            Log::channel('stripe')->info(['paymentMethodIntent' => $paymentMethod->card]);
            $cardExpiry = $paymentMethod->card->exp_year.'-'.$paymentMethod->card->exp_month.'-01';

            $user->update(['card_expiry' => $cardExpiry]);

            Session::put('success',true);
            Session::flash('message', 'Subscription is completed.');

//            Log::channel('stripe')->info(['stripe_success' => 'Subscription is completed for the user_id '.$user->id.'']);
            return back();

        } catch (Exception $e) {


            Log::channel('stripe')->info(['stripe_error' => $e->getMessage(), 'user_id' => $user->id]);
            return back();
        }
    }

    public function cancelSubscription()
    {
        $user = auth()->user();
        $user->subscription($user->subscriptions()->active()->first()->name)->cancel();
        return response()->json([
            'subscription' => $user->subscriptions()->active()->first(),
            'success' => true
        ]);
    }

    public function resumeSubscription()
    {
        $user = auth()->user();
        $user->subscription($user->subscriptions()->active()->first()->name)->resume();
        return response()->json([
            'subscription' => $user->subscriptions()->active()->first(),
            'success' => true
        ]);
    }

    public function changePlan(Request $request)
    {
        $user = auth()->user();
        $activeSubscription = $user->subscriptions()->active()->first();

        if($activeSubscription)
        {
            $activeSubscriptionName = $activeSubscription->name;
            $user->subscription($activeSubscriptionName)->cancelNow();
        }

        if(count($user->paymentMethods()) > 0)
        {
            $paymentPayload = $user->paymentMethods()[0]->id;
        }
        $plan = SubscriptionPlan::find($request->id);

        try
        {
//            creating user on stripe
            if (is_null($user->stripe_id)) {
                $user->createAsStripeCustomer();
            }

//            creating subscription for user
            $user->newSubscription($plan->name, $plan->stripe_price_id)
                ->create($paymentPayload, [
                    'email' => $user->email,
                ]);

            return response()->json([
                'subscription' => auth()->user()->subscriptions()->active()->first()->name,
                'success' => true
            ]);

        } catch (Exception $e) {

            Log::channel('stripe')->info(['stripe_change_plan_error' => $e->getMessage(), 'user_id' => $user->id]);
            return response()->json([
                'error' => $e->getMessage(),
                'success' => false
            ]);
        }
    }
}
