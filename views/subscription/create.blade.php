<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Styles -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <style>
            .alert.parsley {
                margin-top: 5px;
                margin-bottom: 0px;
                padding: 10px 15px 10px 15px;
            }
            .check .alert {
                margin-top: 20px;
            }
            .credit-card-box .panel-title {
                display: inline;
                font-weight: bold;
            }
            .credit-card-box .display-td {
                display: table-cell;
                vertical-align: middle;
                width: 100%;
                text-align: center;
            }
            .credit-card-box .display-tr {
                display: table-row;
            }

            /*Loader Styling*/
            /* Absolute Center Spinner */
            .loading {
                position: fixed;
                z-index: 999;
                overflow: show;
                margin: auto;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                width: 50px;
                height: 50px;
            }

            /* Transparent Overlay */
            .loading:before {
                content: '';
                display: block;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(255,255,255,0.5);
            }

            /* :not(:required) hides these rules from IE9 and below */
            .loading:not(:required) {
                /* hide "loading..." text */
                font: 0/0 a;
                color: transparent;
                text-shadow: none;
                background-color: transparent;
                border: 0;
            }

            .loading:not(:required):after {
                content: '';
                display: block;
                font-size: 10px;
                width: 50px;
                height: 50px;
                margin-top: -0.5em;

                border: 15px solid rgba(33, 150, 243, 1.0);
                border-radius: 100%;
                border-bottom-color: transparent;
                -webkit-animation: spinner 1s linear 0s infinite;
                animation: spinner 1s linear 0s infinite;


            }

            /* Animation */
            @-webkit-keyframes spinner {
                0% {
                    -webkit-transform: rotate(0deg);
                    -moz-transform: rotate(0deg);
                    -ms-transform: rotate(0deg);
                    -o-transform: rotate(0deg);
                    transform: rotate(0deg);
                }
                100% {
                    -webkit-transform: rotate(360deg);
                    -moz-transform: rotate(360deg);
                    -ms-transform: rotate(360deg);
                    -o-transform: rotate(360deg);
                    transform: rotate(360deg);
                }
            }
            @-moz-keyframes spinner {
                0% {
                    -webkit-transform: rotate(0deg);
                    -moz-transform: rotate(0deg);
                    -ms-transform: rotate(0deg);
                    -o-transform: rotate(0deg);
                    transform: rotate(0deg);
                }
                100% {
                    -webkit-transform: rotate(360deg);
                    -moz-transform: rotate(360deg);
                    -ms-transform: rotate(360deg);
                    -o-transform: rotate(360deg);
                    transform: rotate(360deg);
                }
            }
            @-o-keyframes spinner {
                0% {
                    -webkit-transform: rotate(0deg);
                    -moz-transform: rotate(0deg);
                    -ms-transform: rotate(0deg);
                    -o-transform: rotate(0deg);
                    transform: rotate(0deg);
                }
                100% {
                    -webkit-transform: rotate(360deg);
                    -moz-transform: rotate(360deg);
                    -ms-transform: rotate(360deg);
                    -o-transform: rotate(360deg);
                    transform: rotate(360deg);
                }
            }
            @keyframes spinner {
                0% {
                    -webkit-transform: rotate(0deg);
                    -moz-transform: rotate(0deg);
                    -ms-transform: rotate(0deg);
                    -o-transform: rotate(0deg);
                    transform: rotate(0deg);
                }
                100% {
                    -webkit-transform: rotate(360deg);
                    -moz-transform: rotate(360deg);
                    -ms-transform: rotate(360deg);
                    -o-transform: rotate(360deg);
                    transform: rotate(360deg);
                }
            }
        </style>
        <!-- JavaScripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    </head>
    <body id="app-layout">
        <div class="container">
            <div class="row" style="margin-top: 100px;">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default credit-card-box">
                        <div class="panel-heading display-table" >
                            <div class="row display-tr" >
                                <h3 class="panel-title display-td" >Payment Details</h3>
                                <div class="display-td" >
                                    <img src="https://www.merchantequip.com/image/?logos=v|m|a&height=32" alt="Merchant Equipment Store Credit Card Logos"/>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <form method="POST" action="{{url('system/checkout')}}" accept-charset="UTF-8" data-parsley-validate id="checkout-form">
                                    @csrf
                                    {{--@if ($message = Session::get('success'))
                                        <div class="alert alert-success alert-block">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    @endif--}}
                                    <div class="form-group" id="product-group">
                                        <label for="plan">Select Plan:</label>
                                        <select class="form-control" required="required" data-parsley-class-handler="#product-group" id="plan" name="plan">
                                            @foreach($plans as $plan)
                                                <option value="{{$plan->stripe_price_id}}">{{$plan->name.' ('.$plan->currency->code.' '.$plan->price / 100 .')'}}</option>
                                            @endforeach
                                        </select>

                                        <input type="hidden" name="company_id" value="" id="company_id" />
                                        <input type="hidden" name="payment-method" value="" id="payment-method" />
                                    </div>

                                    <div class="form-group">
                                        <label for="card-holder-name">Name On Card:</label>
                                        <input type="text" required="required" class="form-control" id="card-holder-name" />
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div id="card-element"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button id="card-button" class="btn btn-lg btn-block btn-success btn-order">Checkout</button>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span class="payment-errors" id="card-errors" style="color: red;margin-top:10px;"></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="loader" class=""></div>
        </div>
        <!-- PARSLEY -->
        <script>
            window.ParsleyConfig = {
                errorsWrapper: '<div></div>',
                errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
                errorClass: 'has-error',
                successClass: 'has-success'
            };
        </script>

        <script src="//parsleyjs.org/dist/parsley.js"></script>

        <script src="https://js.stripe.com/v3/"></script>
        <script>
            $(document).ready(function(){

                let stripeMsgError='<?php echo Session::get("error")	;?>';
                if(stripeMsgError== 1)
                {
                    document.getElementById('loader').className = '';
                }
                let stripeMsg='<?php echo Session::get("success")	;?>';
                if(stripeMsg== 1)
                {
                    localStorage.setItem("subscribe","true");
                    document.getElementById('loader').className = 'loading';
                    window.location.href = '/system/dashboard';
                }

                let style = {
                    base: {
                        color: '#32325d',
                        lineHeight: '18px',
                        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#aab7c4'
                        }
                    },
                    invalid: {
                        color: '#fa755a',
                        iconColor: '#fa755a'
                    }
                };

                let stripe = Stripe('pk_test_51JPNQNBndor9OArAmEEI5pP8hQLfn7TMuclKSQXyLTxpD9rTIYV6o9USOjtNc5yd6i0kjHbt7EszFMFbJ4zNGT2X00rrJFPf1e', { locale: 'en' });
                let elements = stripe.elements(); // Create an instance of Elements.
                let card = elements.create('card', { style: style }); // Create an instance of the card Element.
                card.mount('#card-element'); // Add an instance of the card Element into the `card-element` <div>.

                card.on('change', function(event) {
                    var displayError = document.getElementById('card-errors');
                    if (event.error) {
                        displayError.textContent = event.error.message;
                    } else {
                        displayError.textContent = '';
                    }
                });

                let paymentMethod = null
                $('#checkout-form').on('submit', function(e){
                    if(paymentMethod)
                    {
                        return true
                    }
                    stripe.confirmCardSetup(
                        "{{$intent->client_secret}}", {
                            payment_method: {
                                card: card,
                                billing_details: { name: $('#card-holder-name').val() }
                            }
                        }
                    ).then(function(result){
                        document.getElementById('loader').className = 'loading';
                        if(result.error)
                        {
                            var errorElement = document.getElementById('card-errors');
                            errorElement.textContent = result.error.message;
                            document.getElementById('loader').className = '';
                        } else {

                            paymentMethod = result.setupIntent.payment_method
                            $('#payment-method').val(paymentMethod)

                            let company_id = localStorage.getItem('selectedCompany');

                            document.getElementById('company_id').value = company_id;

                            $('#checkout-form').submit()
                        }
                    })
                    return false
                })
            });
        </script>
    </body>
</html>
