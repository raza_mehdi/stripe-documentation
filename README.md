# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Introduction

Faktura.dk is a web & mobile app that helps you track expenses, payments & create professional invoices & estimates.

Web Application is made using Laravel & VueJS while the Mobile Apps are built using React Native.

# Table of Contents

1. [Subscription](#subscription)
2. [Subscription Changing](#subscription-swapping)
3. [Subscription Resume](#subscription-resume)
4. [Subscription Cancel](#subscription-cancel)
5. [Subscription Plan](#subscription-plan)
5. [Subscription WebHooks](#subscription-webHooks)

## Subscription

This module will guide you on how to integrate stripe and use it for payments.
Add the following code to your controller. The code is available at /controller/SubscriptionController file.

        $plans = SubscriptionPlan::with('currency')->whereNotIn('name', ['Free'])->get();
        $intent = $user->createSetupIntent();
        return view('subscription.create', compact('plans', 'intent'));

After that copy the following code from /views/subscription/create.blade.php file. 

Later you need to add the code for checkout process. The code is available at 
/controller/SubscriptionController/checkout

        $input = $request->all();
        $paymentPayload = $request->input('payment-method');

        $plan = SubscriptionPlan::whereStripePriceId($input['plan'])->first();

        try {
            /*creating user on stripe*/
            if (is_null($user->stripe_id)) {
                $user->createAsStripeCustomer();
            }

            /*creating subscription for user*/
            $user->newSubscription($plan->name, $input['plan'])
                ->create($paymentPayload, [
                    'email' => $user->email,
                ]);

            /*update card expiry*/
            $paymentMethod = $user->defaultPaymentMethod();

            $cardExpiry = $paymentMethod->card->exp_year.'-'.$paymentMethod->card->exp_month.'-01';

            $user->update(['card_expiry' => $cardExpiry]);

            Session::put('success',true);
            Session::flash('message', 'Subscription is completed.');
            return back();

        } catch (Exception $e) {

            /*for testing*/
            \Mail::to('YOUR-EMAIL-ADDRESS')->send(new subscriptionFailed($data));

            Session::put('error',true);
            Session::flash('message', $e->getMessage());

            Log::channel('stripe')->info(['stripe_error' => $e->getMessage(), 'user_id' => $user->id]);
            return back();
        }

![ScreenShot](images/subscriptionForm.png)

## Subscription Changing

This module will guide you on how to integrate stripe subscription swapping for different accounts.
Add the following code to your controller. The code is available at /controller/SubscriptionController file.

        $user = YOUR AUTHENTICATED LOGIN USER;
        $activeSubscription = $user->subscriptions()->active()->first();

        if($activeSubscription)
        {
            $activeSubscriptionName = $activeSubscription->name;
            $user->subscription($activeSubscriptionName)->cancelNow();
        }

        if(count($user->paymentMethods()) > 0)
        {
            $paymentPayload = $user->paymentMethods()[0]->id;
        }
        $plan = SubscriptionPlan::find($request->id);

        try
        {
            // creating user on stripe
            if (is_null($user->stripe_id)) {
                $user->createAsStripeCustomer();
            }

            // creating subscription for user
            $user->newSubscription($plan->name, $plan->stripe_price_id)
                ->create($paymentPayload, [
                    'email' => $user->email,
                ]);

            return response()->json([
                'subscription' => auth()->user()->subscriptions()->active()->first()->name,
                'success' => true
            ]);

        } catch (Exception $e) {

            Log::channel('stripe')->info(['stripe_change_plan_error' => $e->getMessage(), 'user_id' => $user->id]);
            return response()->json([
                'error' => $e->getMessage(),
                'success' => false
            ]);
        }
    }

## Subscription Resume

This module will guide you on how to integrate stripe subscription resuming for different accounts.
Add the following code to your controller. The code is available at /controller/SubscriptionController file.

        $user = auth()->user();
        $user->subscription($user->subscriptions()->active()->first()->name)->resume();
        return response()->json([
            'subscription' => $user->subscriptions()->active()->first(),
            'success' => true
        ]);

## Subscription Cancel

This module will guide you on how to integrate stripe subscription cancelling for different accounts.
Add the following code to your controller. The code is available at /controller/SubscriptionController file.

        $user = auth()->user();
        $activeSubscription = $user->subscriptions()->active()->first();

        if($activeSubscription)
        {
            $activeSubscriptionName = $activeSubscription->name;
            $user->subscription($activeSubscriptionName)->cancelNow();
        }

        if(count($user->paymentMethods()) > 0)
        {
            $paymentPayload = $user->paymentMethods()[0]->id;
        }
        $plan = SubscriptionPlan::find($request->id);

        try
        {
            // creating user on stripe
            if (is_null($user->stripe_id)) {
                $user->createAsStripeCustomer();
            }

            // creating subscription for user
            $user->newSubscription($plan->name, $plan->stripe_price_id)
                ->create($paymentPayload, [
                    'email' => $user->email,
                ]);

            return response()->json([
                'subscription' => auth()->user()->subscriptions()->active()->first()->name,
                'success' => true
            ]);

        } catch (Exception $e) {

            Log::channel('stripe')->info(['stripe_change_plan_error' => $e->getMessage(), 'user_id' => $user->id]);
            return response()->json([
                'error' => $e->getMessage(),
                'success' => false
            ]);
        }
    }

## Subscription Plan

This module will guide you on how to integrate stripe subscription plan adding for different accounts.
Add the following code to your controller. The code is available at /controller/SubscriptionPlanController file.

        $nameData['billing_type'] = $request->billing_type['name'];
        $currency_code = Currency::findOrFail($request->currency['id']);

        $stripe = new \Stripe\StripeClient(
            Config::get('YOUR STRIPE SECRET')
          );

        $product = $stripe->products->create([
        'name' => $request->name,
        'description' => $request->description,
        ]);

        if($nameData['billing_type'] == 'YOUR REQUIRED BILLING TYPE'){

            $duration= [
                'unit_amount' => $request->price*100,
                'currency' => strtolower($currency_code['code']),
                'product' => $product->id
            ];
        }else{
    
            $duration=  [
                'unit_amount' => $request->price*100,
                'currency' => strtolower($currency_code['code']),
                'recurring' => ['interval' => 'month'],
                'product' => $product->id
            ];
        }

        if(isset($product->id)){
          $price =   $stripe->prices->create([$duration]);
        }

        if(isset($price->id)){
            $subscription = new SubscriptionPlan();

            $subscription->name = $request->name;
            $subscription->price = $request->price*100;
            $subscription->description = $request->description;
            $subscription->billing_type = $request->billing_type['name'];

            if(isset($price->recurring)){
                $subscription->billing_period = $price->recurring->interval;
            }

            $subscription->currency_id = $request->currency['id'];
            $subscription->stripe_price_id = $price->id;
            $subscription->stripe_product_id = $product->id;

            $subscription->save();
        }
        $subscription = SubscriptionPlan::find($subscription->id);

        return response()->json([
            'status' => 200,
            'success' => true,
            'plans' => $subscription
        ]);

Also you need to add the frontend functionality for this. The frontend code is available at 
/views/plans.vue

![ScreenShot](images/Plans.png)


## Subscription WebHooks

This module will guide you on how to integrate stripe subscription webHooks for different accounts.
Add the following code to your controller. The code is available at /controller/WebHookActivityController file.

        //signing secret
        $endpoint_secret =  Config::get('YOUR-SIGNIN-SECRET');

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        }

        $obj = $event->data->object; // contains a \Stripe\PaymentIntent

        switch ($event->type) {
            case 'charge.succeeded':
                Log::channel('stripe')->info(['payment succeed response' => $obj]);
                $user = User::whereStripeId($obj['customer'])->first();
                if($user)
                {
                    $subscription = Subscription::where(['user_id' => $user->id, 'stripe_status' => 'active'])->whereNull('ends_at')->first();

                    // deactivate previous subscription to maintain subscription history
                    SubscriptionHistory::where('user_id', $user->id)->update(['active' => 'NO']);

                    // create new subscription entry
                    SubscriptionHistory::create([
                        'user_id'       => $user->id,
                        'name'          => $subscription->name,
                        'stripe_id'     => $subscription->stripe_id,
                        'payment_id'    => $obj['id'],
                        'subtotal'      => $obj['amount'],
                        'tax'           => 0,
                        'total'         => $obj['amount'],
                        'receipt_url'   => $obj['receipt_url'],
                        'company_id'    => $user->company_id,
                        // 'currency_id'   => $user->currency_id,
                    ]);
                    Log::channel('stripe')->info(['stripe payment has been successfully added against user_id' => $user->id]);
                }
                break;

            case 'charge.failed':
                $user = User::whereStripeId($obj['customer'])->first();
                Log::channel('stripe')->info(['charge failed against user' => $obj['customer']]);
                if($user)
                {
                    $subscription = Subscription::where(['user_id' => $user->id, 'stripe_status' => 'active'])->whereNull('ends_at')->first();
                    if($subscription)
                    {
                        $user->subscription($subscription->name)->cancelNow();
                        Log::channel('stripe')->info(['stripe payment failed against user_id' => $user->id]);
                    }
                } else {
                    Log::channel('stripe')->info(['stripe payment failed due to invalid card details']);
                }
                break;
            case 'customer.subscription.deleted':
                Subscription::where('stripe_id', $obj['id'])->update(['stripe_status' => 'canceled', 'ends_at' => now()]);
                Log::channel('stripe')->info(['subscription cancelled against subscription_id' => $obj['id']]);
                break;
            default:
                Log::channel('stripe')->info(['Received unknown event type ' . $event->type]);
        }
        http_response_code(200);
    }

### What is this repository for? ###

* Here you can get all the code and information required to successfully integrate the stripe into your system.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/raza_mehdi/stripe-documentation/src/master/)

### How do I get set up? ###

- You just need to follow above instruction to get follow with the integration.
- No extra configuration required
- Install through composer.
    ![ScreenShot](images/composer.png)

  
- You need to create following databse tables to successfully run the integration

    ![ScreenShot1](images/subscriptions.png)
    ![ScreenShot2](images/subscriptionHistory.png)
    ![ScreenShot3](images/subscriptionPlans.png)

- Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact